# warp-identifier

| WARP     | <WARP number>
|----------|--------------------------------------------------------
| Title    | <title of this proposal>
| Author   | <authors' real names and email addresses>
| Type     | <Standards, Process, or Informational>
| Proposed | <date of original proposal, DD-Mon-YYYY>
| Status   | Draft

----

# Summary
[summary]: #summary

This is a short (~200 word) description of the issue being addressed.

# Motivation
[motivation]: #motivation

This section should the reasons _why_ this issue needs to be addressed.

Since changes to the core processes and tools of Weld could have surprising
or unpleasant effects on its community, it's important to to clearly explain
why the current specification is inadequate to address the problem that this
WARP will set out to solve.

# Specification
[spec]: #spec

This section lays out the details of what's being proposed. This should include
full documentation of APIs, descriptions of use cases or user interfaces,
file format or language specifications, and the like.

# Rationale
[rationale]: #rationale

This section should describe what other designs have been considered, possible
downsides of the proposed design, and other such considerations.

You may also want to break these things out into subsections, like below.

## Considerations
[considerations]: #considerations

Here you would describe the various factors that were considered in the design
process and why you made the decisions that you did.

## Alternatives
[alternatives]: #alternatives

This subsection would describe other implementations, and how this specification
is similar (or different) from them. It should also explain why the solution
described in this specification is a better choice than the alternatives.

## Unresolved Questions
[questions]: #questions

Here you'd list any open questions that remain for future research, including
possible problems and pitfalls.

# Copyright
[copyright]: #copyright

This work is licensed under the Creative Commons Attribution 4.0 International
License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/.
