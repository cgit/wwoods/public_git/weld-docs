# weld-amendment-review-process

| WARP     | 1
|----------|------------------------------
| Title    | Weld Amendment Review Process
| Author   | Will Woods (<wwoods@redhat.com>)
| Type     | Process
| Proposed | 20 Jan 2016
| Status   | Draft

----

# Summary
[summary]: #summary

This document describes the process for making changes to Weld's design via
documents called WARPs. WARP stands for Weld Amendment Review Process.

A WARP is a design document that describes the processes, tools, protocols and
metadata used by the Weld community and developers.

# Motivation
[motivation]: #motivation

Decades of haphazard management of existing Linux distributions has led to
mountains of technical debt, with angry, tone-deaf communities of developers and
users unable to make substantive changes to anything without months of vitriolic
argument. This leads either to burnout, or to changes being made behind closed
doors to avoid arguments.

We want instead to be deliberate, thoughtful, and thoroughly transparent in
making changes or extensions to the distribution and its tools, interfaces, and
processes.

We are inspired by Python's [PEP]s, [Rust RFC]s, the IETF's [RFC] process, and
other similar community processes for making changes and publishing technical
details in an orderly, transparent way.

[RFC]: https://www.ietf.org/rfc.html
[PEP]: https://www.python.org/dev/peps/
[Rust RFC]: https://github.com/rust-lang/rfcs

# Specification
[spec]: #spec

## WARP files

WARP files are to be written in [GitHub-flavored Markdown] and stored in a
public version control system (like, say, git) that will preserve all the
changes for posterity.

[GitHub-flavored Markdown]: https://help.github.com/articles/github-flavored-markdown/

WARP files should usually have the following sections, although they don't
always need to be in this order.

1. **Header**: basic information about this WARP and its status; see below.
2. **Summary**: a short summary of the purpose of this WARP.
3. **Motivation**: describes the reasons this WARP is needed.
4. **Specification**: complete details of the implementation of this WARP.
5. **Rationale**: fleshes out the specification by describing what motivated its
   design. In particular, this section should describe:
   * _Considerations_: what factors influenced the choices made in this design?
     Are there any possible downsides to these choices?
   * _Alternatives_: what other implementations exist? What are their strengths
     and weaknesses compared to this one?
   * _Unresolved Questions_: are there any parts of the problem left unresolved,
     or any areas where further research might be needed?
6. **Copyright**: either public domain or CC-BY 4.0.

### WARP Header block

Here's a WARP Header template. Rows marked with `*` are optional; others are
required.

```
# <identifier, e.g. weld-amendment-review-process>

| WARP     | <WARP number>
|----------|--------------------------------------------------------
| Title    | <title>
| Author   | <list of author's real names and email addresses>
| Type     | <Standards, Process, or Informational>
| Proposed | <date of original proposal>
| Status   | <Draft, Accepted, Rejected, Withdrawn, Obsoleted>

----
```

(The astute reader will notice that this is a Markdown H1 header, followed by a
[GitHub-flavored Markdown]-style table.)

#### WARP Types

1. A **Standards** WARP describes an API, protocol, or file format used by Weld.
   These standards define the technical details of the Weld project.
   They describe _what we're building, and how it works._
2. **Process** WARPs describe the processes that surround the Weld project and
   help to define the Weld community's rights and responsibilities.
   They describe _how we work on Weld and how you can work with us._
3. **Informational** WARPs provide guidelines or information to the Weld
   community about Weld design philosophies, goals, or other issues.
   They describe _how Weld works best_.

#### Status
[status]: #status

The **Status** field can have one of these values:
1. **Draft**: still being drafted by the authors, may change
   without warning.
2. **Accepted**: has been officially accepted by the Project Lead.
3. **Rejected**: rejected by the Project Lead.
4. **Withdrawn**: withdrawn by the authors.
5. **Obsoleted**: obsoleted by a later WARP (a link should be present)

## WARP Workflows

Here are the processes for dealing with WARPs.

### Proposing a new WARP

* Clone `weld-docs`
* Pick an identifier
* Copy `warp/0000-template.md` to `warp/XXXX-your-identifier.md`
* Fill it in
* Send a PR
* That's about it

### Reporting WARP bugs

* Minor bugs (typos, broken links, etc.) can just be sent as PRs
* Major design issues with **Draft** WARPs should be discussed with authors
* Major design issues with **Accepted** WARPs should be handled by proposing
  a new WARP which amends or obsoletes the old one.

# Rationale
[rationale]: #rationale

## Considerations
[considerations]: #considerations

1. We should be public and transparent.

   This is just basic Open Source Citizenship!

2. We need to retain historical records and the reasoning behind things.

   The [Fedora Packaging Guidelines], for example, don't give any indication
   of why any of the rules exist, or notes about rules that have been updated
   or removed.

3. We should not depend on a particular website or service.

   Decentralized is better; people should be able to fork the entire project
   (process, governance, documentation, and all) if they so choose.

[Fedora Packaging Guidelines]: https://fedoraproject.org/wiki/Packaging:Guidelines

## Alternatives
[alternatives]: #alternatives

* The IETF [RFC] process is a little too rigid and formal for a project this
  young.
* The [Rust RFC] process is tied a little too tightly to GitHub.
* The [PEP] process is a little too Python-specific.
* Fedora-style processes are a wacky hodgepodge: some parts are stiff and formal
  ([Fedora Packaging Guidelines]), and the rest varies wildly depending on which
  team/person owns it.
* TODO: Review Fedora's Feature process

## Unresolved Questions
[questions]: #questions

* Who approves WARPs?
  * Project governance will be defined in a future WARP.
  * Until then, **Will is the Stark Fist of Authority for all things Weld.**
* How do we know when a WARP has actually been _implemented_?
  * Probably need to add a field to the Header?
* IETF [RFC]s use MUST, SHOULD, etc. as keywords, with meanings defined in
  [RFC 2119]. We MAY (heh) want to adopt these conventions?
* How do we handle references between WARPs?
  * uhhhh does this work? [WARP 0](0000-template.md)
    * Probably only on github

[RFC 2119]: https://www.ietf.org/rfc/rfc2119.txt

# Copyright
[copyright]: #copyright

This work is licensed under the Creative Commons Attribution 4.0 International
License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/.
