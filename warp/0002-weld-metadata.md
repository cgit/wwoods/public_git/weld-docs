# weld-pkg-metadata

| WARP     | 2
|----------|--------------------------------------------------------
| Title    | Weld Package Metadata
| Author   | Will Woods
| Type     | Standards
| Proposed | 21 Jan 2016
| Status   | Draft

----

# Summary
[summary]: #summary

This document describes the package metadata used by the Weld project.

# Motivation
[motivation]: #motivation

The heart of a Linux distribution is its package metadata. [RPM]-based
distributions have [spec files], [dpkg]-based distributions
have the [`debian/` directory], [Gentoo] has [ebuild] files, and so on.

Since everything produced by a Linux distribution is derived from this metadata,
it's crucial to the health of the project. Weld's metadata is designed with
the following goals in mind:

1. **Simple**: the files should be short and (mostly) human-readable.
2. **Flexible**: we should be able to add new fields when needed.
3. **Powerful**: the metadata should be able to express everything currently
   handled by [spec files] in Fedora.
4. **Customizable**: it should be easy for a Weld user to fork and build their
   own variant, with different build flags, alternative implementations, etc.

[RPM]: http://rpm.org/
[spec files]: http://www.rpm.org/max-rpm/ch-rpm-inside.html
[dpkg]: https://en.wikipedia.org/wiki/Dpkg
[`debian/` directory]: https://www.debian.org/doc/manuals/maint-guide/dreq.en.html
[Gentoo]: https://www.gentoo.org/
[ebuild]: https://devmanual.gentoo.org/ebuild-writing/index.html

[YUM metadata]: https://en.opensuse.org/openSUSE:Standards_Rpm_Metadata
[Fedora RPM Guide]: https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/ch-specfiles.html

# Specification
[spec]: #spec

## File format

Weld's metadata files are in [TOML] format, with sections, keys, and values
to be described below.

[TOML]: https://github.com/toml-lang/toml

## File names and repo layout

The files are named `<package>.weld`, and they are kept in a git repo named
`weld-dist`. Related packages may grouped into subdirectories - for example,
`gfx/wayland.weld` or `dev/gcc.weld`.

## File sections

### `[upstream]`

The `[upstream]` section describes where the upstream code can be found, what
format it's in, and other metadata pertaining to finding and fetching code
from the upstream developers.

#### `homepage = <http-url>`

_Optional._ Gives a `http` or `https` URL to the upstream project's homepage.

#### `vcs = <url>`

Gives the URL of the upstream's version control system.

#### `tarball = <url>`

Gives the URL to a tarball of released sources.

The escape sequence `{version}` can be used as a placeholder for the version
string.

### `[build]`

The `[build]` section describes how to build the software

# Rationale
[rationale]: #rationale

## Considerations
[considerations]: #considerations

One of the major goals for Weld is that package metadata and build instructions
should be encoded as _data_, not _code_ - in other words: no shell scripts
unless absolutely necessary.

[TOML] was chosen because of its clean, simple syntax and unambiguous parsing
rules. This allows `.weld` files to be read and written easily in nearly any
popular programming language.

## Alternatives
[alternatives]: #alternatives

### `spec`

* Under-documented - where's the specification for spec files?
* Inflexible - no `TestRequires`, and no easy way to add it
* Opaque - impossible to tell what scriptlets will do, how long they will take,
  how much disk they might use, etc.

### `debian/*`

TODO

### `ebuild`

Also TODO

### `Cargo.toml` and `Cargo.lock`

TODO. A lot of what's going on here is inspired by (or coincidentally
similar to) [Cargo] and [Cargo manifest]s; look closer at it.

[Cargo]: http://doc.crates.io/guide.html
[Cargo manifest]: http://doc.crates.io/manifest.html

## Unresolved Questions
[questions]: #questions

* How are we gonna create the initial set of `.weld` files?
* What escape sequences do we want/need?
  * And what form should those take? `{name}`, `%n`, ..?
* If we're gonna talk about dependencies we need a spec for package / binary
  match expressions
  * like RPM version expressions (`xz >= 5.2.0`)
  * or Cargo version expressions ()

# Copyright
[copyright]: #copyright

This work is licensed under the Creative Commons Attribution 4.0 International
License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/.
